import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;


public class AC_BTCe_BitStamp {
	static BigDecimal totalCost;
	
	
	public static BigDecimal getTotalCost()
	{
		return totalCost;
	}
	public static BigDecimal run() throws IOException {
			OrderBook BTCeBook = OrderBookGetter.getBTCeBook();
			OrderBook BitStamp = OrderBookGetter.getBitStampBook();
			
			if(BTCeBook.arbitroll(BitStamp, new BigDecimal(100)).compareTo(BitStamp.arbitroll(BTCeBook, new BigDecimal(100))) == -1)
			{
				String a = BitStamp.arbitroll(BTCeBook, new BigDecimal(100)).toString();
				totalCost = BitStamp.getTotalCost();
				
				File csvData = new File("AC_BTCe_BitStamp.csv");
				if(!csvData.exists()) {
				    csvData.createNewFile();
				} 
				FileWriter fw = new FileWriter(csvData);
				totalCost = BitStamp.getTotalCost();
				fw.write(a + "," + totalCost + "," + System.currentTimeMillis());
				fw.close();
				return new BigDecimal(a);
			}
			else
			{
				String b = BTCeBook.arbitroll(BitStamp, new BigDecimal(100)).toString();
				
				File csvData = new File("AC_BTCe_BitStamp.csv");
				if(!csvData.exists()) {
				    csvData.createNewFile();
				}
				FileWriter fw = new FileWriter(csvData);
				totalCost = BTCeBook.getTotalCost();
				fw.write(b + "," + totalCost + "," + System.currentTimeMillis());
				fw.close();
				return new BigDecimal(b);
			}
			
			
		
	}//end run()
	
}
