import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;


public class AC_BitStamp_MtGox {
	static BigDecimal totalCost;
	
	public static BigDecimal getTotalCost()
	{
		return totalCost;
	}
	
	public static BigDecimal run() throws IOException {
			OrderBook BitStampBook = OrderBookGetter.getBitStampBook();
			OrderBook MtGoxBook = OrderBookGetter.getMtGoxBook();
			String a = BitStampBook.arbitroll(MtGoxBook, new BigDecimal(100)).toString();
			
			
			File csvData = new File("AC_BitStamp_MtGox.csv");
			if(!csvData.exists()) {
			    csvData.createNewFile();
			} 
			FileWriter fw = new FileWriter(csvData);
			totalCost = BitStampBook.getTotalCost();
			fw.write(a + "," + totalCost + "," + System.currentTimeMillis());
			fw.close();
			
			return new BigDecimal(a);
			
			
		
		
	}//end run()

}
