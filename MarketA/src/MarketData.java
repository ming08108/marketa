import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;


public class MarketData {

	private static JsonObject readFrom(String URL)
	{
		URL ticker = null;
		URLConnection UC = null;
		try {
			ticker = new URL(URL);
			UC = ticker.openConnection();
			UC.setRequestProperty("User-Agent", "I am a BOT lolo");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("invalid URL - MalformedURLException");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException URL");
			e.printStackTrace();
		}
		BufferedReader bf = null;
		JsonObject data = null;
		try {
			bf = new BufferedReader(new InputStreamReader(UC.getInputStream()));
			data = JsonObject.readFrom(bf.readLine());
		} catch (IOException e) {
			System.out.println("IOException BufferedReader");
			e.printStackTrace();
			return null;
		}
		
		return data;
	}
	
	public static OrderBook getMtGoxBook()
	{
		OrderBook book = new OrderBook();
		JsonObject data = readFrom("http://data.mtgox.com/api/2/BTCUSD/money/depth/fetch"); //get book data
		
		JsonArray asks = data.get("data").asObject().get("asks").asArray(); //get bids as JsonArray
		JsonArray bids = data.get("data").asObject().get("bids").asArray(); //get asks as JsonArray
		for(JsonValue v : asks)
		{
			BigDecimal price = BigDecimal.valueOf(v.asObject().get("price").asDouble());
			BigDecimal amount = BigDecimal.valueOf(v.asObject().get("amount").asDouble());
			if(!(amount.compareTo(new BigDecimal(0)) == 0))
			{
				book.addAskInfo(price, amount);
			}
		}
		
		for(int i = bids.size()-1; i >= 0; i--)
		{
			BigDecimal price = BigDecimal.valueOf(bids.get(i).asObject().get("price").asDouble());
			BigDecimal amount = BigDecimal.valueOf(bids.get(i).asObject().get("amount").asDouble());
			if(!(amount.compareTo(new BigDecimal(0)) == 0))
			{
				book.addBidInfo(price, amount);
			}
		}
		
		return book;
		
	}
	
	public static BigDecimal getMtGox()
	{
		
		JsonObject data = readFrom("http://data.mtgox.com/api/1/BTCUSD/ticker_fast");
		
		if(data == null)
		{
			return new BigDecimal(-1);
		}
		
		BigDecimal price = BigDecimal.valueOf(Double.valueOf(data.get("return").asObject().get("last_local").asObject().get("value").asString()));
		return price;
	}//end getMtGox
	
	public static OrderBook getBitStampBook()
	{
		OrderBook book = new OrderBook();
		JsonObject data = readFrom("https://www.bitstamp.net/api/order_book/"); //get book data
		
		
		
		JsonArray asks = data.get("asks").asArray(); //get bids as JsonArray
		JsonArray bids = data.get("bids").asArray(); //get asks as JsonArray
		for(JsonValue v : asks)
		{
			BigDecimal price = new BigDecimal(v.asArray().get(0).asString());
			BigDecimal amount = new BigDecimal(v.asArray().get(1).asString());
			if(!(amount.compareTo(new BigDecimal(0)) == 0))
			{
				book.addAskInfo(price, amount);
			}
		}
		
		for(JsonValue v : bids)
		{
			BigDecimal price = new BigDecimal(v.asArray().get(0).asString());
			BigDecimal amount = new BigDecimal(v.asArray().get(1).asString());
			if(!(amount.compareTo(new BigDecimal(0)) == 0))
			{
				book.addBidInfo(price, amount);
			}
		}
		
		return book;
		
	}
	
	public static BigDecimal getBitStamp()
	{
		JsonObject data = readFrom("https://www.bitstamp.net/api/ticker/");
		
		if(data == null)
		{
			return new BigDecimal(-1);
		}
		
		BigDecimal price = BigDecimal.valueOf(Double.valueOf(data.get("last").asString()));
		return price;
	}
	
	public static OrderBook getBTCeBook()
	{
		OrderBook book = new OrderBook();
		JsonObject data = readFrom("https://btc-e.com/api/2/btc_usd/depth");
		
		JsonArray asks = data.get("asks").asArray();
		JsonArray bids = data.get("bids").asArray();
		
		for(JsonValue v : asks){
			BigDecimal price = BigDecimal.valueOf(v.asArray().get(0).asDouble()); //get price
			BigDecimal amount = BigDecimal.valueOf(v.asArray().get(1).asDouble()); //get amount (BTC)
			if(!(amount.compareTo(new BigDecimal(0)) == 0))
			{
				book.addAskInfo(price, amount);
			}
		}
		for(JsonValue v : bids){
			BigDecimal price = BigDecimal.valueOf(v.asArray().get(0).asDouble()); //get price
			BigDecimal amount = BigDecimal.valueOf(v.asArray().get(1).asDouble()); //get amount (BTC)
			if(!(amount.compareTo(new BigDecimal(0)) == 0))
			{
				book.addBidInfo(price, amount);
			}
		}
		
		return book;
	}
	
	public static BigDecimal getBTCe()
	{
		JsonObject data = readFrom("https://btc-e.com/api/2/btc_usd/ticker");
		if(data == null)
		{
			return new BigDecimal(-1);
		}
		BigDecimal price = BigDecimal.valueOf(data.get("ticker").asObject().get("last").asDouble());
		return price;
	}
	
}
