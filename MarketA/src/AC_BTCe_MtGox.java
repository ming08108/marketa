import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;


public class AC_BTCe_MtGox {
	static BigDecimal totalCost;

	public static BigDecimal getTotalCost()
	{
		return totalCost;
	}
	
	public static BigDecimal run() throws IOException {
			OrderBook BTCeBook = OrderBookGetter.getBTCeBook();
			OrderBook MtGoxBook = OrderBookGetter.getMtGoxBook();
			
			String a = BTCeBook.arbitroll(MtGoxBook, new BigDecimal(100)).toString();
			totalCost = BTCeBook.getTotalCost();
			
			File csvData = new File("AC_BTCe_MtGox.csv");
			if(!csvData.exists()) {
			    csvData.createNewFile();
			} 
			FileWriter fw = new FileWriter(csvData);
			totalCost = BTCeBook.getTotalCost();
			fw.write(a + "," + totalCost + "," + System.currentTimeMillis());
			fw.close();
			return new BigDecimal(a);
			
		
	}
}//end AC_BTCe_MtGox