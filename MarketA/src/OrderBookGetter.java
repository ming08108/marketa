import java.io.IOException;


public class OrderBookGetter implements Runnable{
	
	static OrderBook MtGoxBook;
	static OrderBook BitStampBook;
	static OrderBook BTCeBook;
	
	
	public static OrderBook getBitStampBook() {
		return BitStampBook;
	}

	public static void setBitStampBook(OrderBook bitStampBook) {
		BitStampBook = bitStampBook;
	}

	public static OrderBook getBTCeBook() {
		return BTCeBook;
	}

	public static void setBTCeBook(OrderBook bTCeBook) {
		BTCeBook = bTCeBook;
	}

	public static void setMtGoxBook(OrderBook mtGoxBook) {
		MtGoxBook = mtGoxBook;
	}


	

	public static OrderBook getMtGoxBook() {
		return MtGoxBook;
	}

	@Override
	public void run() {
		while(Main.getFlag())
		{
			
			
			MtGoxBook = MarketData.getMtGoxBook();
			BitStampBook = MarketData.getBitStampBook();
			BTCeBook = MarketData.getBTCeBook();
			
			try {
				System.out.println(AC_BTCe_MtGox.run() + " AC_BTCe_MtGox" + " Total Money: " + AC_BTCe_MtGox.getTotalCost());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				System.out.println(AC_BitStamp_MtGox.run() + " AC_BitStamp_MtGox" + " Total Money: " + AC_BitStamp_MtGox.getTotalCost());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				System.out.println(AC_BTCe_BitStamp.run() + " AC_BTCe_BitStamp" + " Total Money: " + AC_BTCe_BitStamp.getTotalCost());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			try {
				Thread.sleep(30 * 60 * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
