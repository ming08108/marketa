import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;


public class MtGox_BTCe implements Runnable
{
	@Override
	public void run(){
		BigDecimal last = new BigDecimal("-1");
		
		File csvData = new File("MtGox_BTCe.csv");
		if(!csvData.exists()) {
		    try {
				csvData.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
		
		FileWriter fw = null;
		try {
			fw = new FileWriter(csvData.getAbsoluteFile(), true);
		} catch (IOException e) {
			System.out.println("IoEXception");
			e.printStackTrace();
		} //create a file write Append = true.
		
		while(Main.getFlag())
		{
			BigDecimal MtGoxLAST = MarketData.getMtGox();//get MtGox Last price
			BigDecimal BTCe = MarketData.getBTCe(); //get bitstamp last price
			
			
			
			if(!last.equals(MtGoxLAST.subtract(BTCe)) && (MtGoxLAST.compareTo(new BigDecimal(-1)) != 0) && (BTCe.compareTo(new BigDecimal(-1))!= 0) ){
				try {
					fw.write(MtGoxLAST + "," + BTCe + "," + (MtGoxLAST.subtract(BTCe)) + "," + (System.currentTimeMillis()/1000) + "\n");
				} catch (IOException e) {
					System.out.println("IOException");
					e.printStackTrace();
				} //write data to file
				System.out.println(MtGoxLAST + " " + BTCe + " " + (MtGoxLAST.subtract(BTCe))); //Print to console
				last = MtGoxLAST.subtract(BTCe);
				
			}//end if
			if((MtGoxLAST.compareTo(new BigDecimal(-1)) == 0) || (BTCe.compareTo(new BigDecimal(-1))== 0))
			{
				System.out.println("Error Data not retrieved successfuly");
			}
			
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}//wait
			
			try {
				fw.flush();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}//end while
		
		try {
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}//close the FileWriter
		
	}

}
