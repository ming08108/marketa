
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;



public class OrderBook {
	ArrayList<ArrayList<BigDecimal>> asks; //offers to sell
	ArrayList<ArrayList<BigDecimal>> bids; //offers to buy
	
	BigDecimal totalCost = new BigDecimal(0);
	
	public void setBids(ArrayList<ArrayList<BigDecimal>> b)
	{
		bids = b;
	}
	public void setAsks(ArrayList<ArrayList<BigDecimal>> a)
	{
		asks = a;
	}
	
	public OrderBook(ArrayList<ArrayList<BigDecimal>> b, ArrayList<ArrayList<BigDecimal>> s){
		asks = b;//offers to sell
		bids = s;//offers to buy
	}
	
	public OrderBook()
	{
		asks = new ArrayList<ArrayList<BigDecimal>>();//offers to sell
		bids = new ArrayList<ArrayList<BigDecimal>>();//offers to buy
	}
	
	public void addAskInfo(BigDecimal price, BigDecimal amount) //price in USD, amount in BTC
	{
		ArrayList<BigDecimal> order = new ArrayList<BigDecimal>();
		order.add(price);
		order.add(amount);
		
		asks.add(order);
	}
	
	public void addBidInfo(BigDecimal price, BigDecimal amount) //price in USD, amount in BTC
	{
		ArrayList<BigDecimal> order = new ArrayList<BigDecimal>();
		order.add(price);
		order.add(amount);
		
		bids.add(order);
	}
	
	public BigDecimal arbitroll(OrderBook o, BigDecimal no)
	{

		BigDecimal totalProfit = new BigDecimal(0);
		ArrayList<ArrayList<BigDecimal>> asksCopyCurrent = new ArrayList<ArrayList<BigDecimal>>();
		for(ArrayList<BigDecimal> a : asks)
		{
			asksCopyCurrent.add(new ArrayList<BigDecimal>(a));
		}
		ArrayList<ArrayList<BigDecimal>> bidsCopyOther = new ArrayList<ArrayList<BigDecimal>>();
		for(ArrayList<BigDecimal> b : o.getBids())
		{
			bidsCopyOther.add(new ArrayList<BigDecimal>(b));
		}
		
		Iterator<ArrayList<BigDecimal>> current = asksCopyCurrent.iterator();
		Iterator<ArrayList<BigDecimal>> other = bidsCopyOther.iterator();
		ArrayList<BigDecimal> currentAsk = current.next();
		ArrayList<BigDecimal> otherBid = other.next();

//while(current.hasNext() && other.hasNext()){
while((currentAsk.get(1).compareTo(new BigDecimal(0)) == 1) && (otherBid.get(1).compareTo(new BigDecimal(0)) == 1)){
	if(currentAsk.get(1).compareTo(otherBid.get(1))==1 && otherBid.get(0).compareTo(currentAsk.get(0)) == 1) {
		while(currentAsk.get(1).compareTo(new BigDecimal(0)) == 1){
			if(otherBid.get(0).compareTo(currentAsk.get(0)) < 1)
				return totalProfit;
			totalCost = totalCost.add(currentAsk.get(0).multiply(otherBid.get(1)));
			totalProfit = totalProfit.add(otherBid.get(0).multiply(otherBid.get(1)).subtract(otherBid.get(1).multiply(currentAsk.get(0))));
			currentAsk.set(1,currentAsk.get(1).subtract(otherBid.get(1)));
			if(other.hasNext())
				otherBid = other.next();
			else
				return totalProfit;
			if(!(currentAsk.get(1).compareTo(new BigDecimal(0)) == 0) && currentAsk.get(1).subtract(otherBid.get(1)).compareTo(new BigDecimal(0)) == -1){
				totalCost = totalCost.add(currentAsk.get(0).multiply(currentAsk.get(1)));
				totalProfit = totalProfit.add(currentAsk.get(1).multiply(otherBid.get(0)).subtract(currentAsk.get(0).multiply(currentAsk.get(1))));
				otherBid.set(1,otherBid.get(1).subtract(currentAsk.get(1)));
				currentAsk.set(1, new BigDecimal(0));
			}
		}
		if(current.hasNext())		
			currentAsk = current.next();
		else
			return totalProfit;
		
	} else if(otherBid.get(1).compareTo(currentAsk.get(1)) == 1 && otherBid.get(0).compareTo(currentAsk.get(0)) == 1 ){
		while(otherBid.get(1).compareTo(new BigDecimal(0)) == 1){
			if(otherBid.get(0).compareTo(currentAsk.get(0)) < 1)
				return totalProfit;
			totalCost = totalCost.add(currentAsk.get(1).multiply(currentAsk.get(0)));	
			totalProfit = totalProfit.add(otherBid.get(0).multiply(currentAsk.get(1)).subtract(currentAsk.get(0).multiply(currentAsk.get(1))));
			otherBid.set(1,otherBid.get(1).subtract(currentAsk.get(1)));
			if(current.hasNext())
				currentAsk = current.next();
			else
				return totalProfit;
			if(!(otherBid.get(1).compareTo(new BigDecimal(0)) == 0) && otherBid.get(1).subtract(currentAsk.get(1)).compareTo(new BigDecimal(0)) == -1){
				totalCost = totalCost.add(currentAsk.get(0).multiply(otherBid.get(1)));				
				totalProfit = totalProfit.add(otherBid.get(1).multiply(otherBid.get(0)).subtract(otherBid.get(1).multiply(currentAsk.get(0))));
				currentAsk.set(1,currentAsk.get(1).subtract(otherBid.get(1)));
				otherBid.set(1,new BigDecimal(0));
				
			}
		}
		if(other.hasNext())
			otherBid = other.next();
		else
			return totalProfit;
	
	} else{
		if(otherBid.get(0).compareTo(currentAsk.get(0)) < 1 )
			return totalProfit;
		totalCost = totalCost.add(currentAsk.get(0).multiply(currentAsk.get(1)));
		totalProfit = totalProfit.add(otherBid.get(0).multiply(currentAsk.get(1)).subtract(currentAsk.get(0).multiply(currentAsk.get(1))));
		if(current.hasNext())
			currentAsk = current.next();
		else
			return totalProfit;
		if(other.hasNext())
			otherBid = other.next();
		else
			return totalProfit;
		}
	}
	return totalProfit;
	}
	
	public BigDecimal getArbitrageMoney(OrderBook o, BigDecimal m) //compare others bids(buy orders) to instance asks(sell orders)
	{
		BigDecimal totalProfit = new BigDecimal(0);
		Iterator<ArrayList<BigDecimal>> current = asks.iterator();
		Iterator<ArrayList<BigDecimal>> other = o.getBids().iterator();
		BigDecimal money = m;
		totalCost = new BigDecimal(0);
		
		
		while(current.hasNext() && other.hasNext() && (money.compareTo(new BigDecimal(0)) == 1))
		{
			ArrayList<BigDecimal> currentAsk = current.next(); //get current ask price
			for(BigDecimal a : currentAsk)
			{
				//System.out.println(a.toString() + " Current Ask");
			}
			ArrayList<BigDecimal> otherBid = other.next(); //get other bid price
			for(BigDecimal a : otherBid)
			{
				//System.out.println(a.toString() + " Other Bid");
			}
			
			if(currentAsk.get(0).compareTo(otherBid.get(0)) == -1) //if ask(sell-order) is less than bid(buy-order)
			{
				if(currentAsk.get(1).compareTo(otherBid.get(1)) == -1)//if ask(sell-order) quantity is less than bid(buy-order)
				{
					
					
					
					BigDecimal cost = currentAsk.get(0).multiply(limitSize(currentAsk.get(1), currentAsk.get(0), money)); //multiply the ask price by the quantity
					
					//System.out.println(cost.toString() + " Cost");
					
					
					BigDecimal revenue = otherBid.get(0).multiply(limitSize(currentAsk.get(1), currentAsk.get(0), money)); //multiply the bid price by the ask quantity
					
					//System.out.println(revenue.toString() + " Revenue");
					
					BigDecimal profit = revenue.subtract(cost);
					
					//System.out.println();
					
					totalCost = totalCost.add(cost);
					totalProfit = totalProfit.add(profit);
					money = money.subtract(cost);
					
				}//end if
				else
				{
					BigDecimal cost = currentAsk.get(0).multiply(limitSize(otherBid.get(1), otherBid.get(0), money)); //multiply the ask price by the lower quantity
					
					//System.out.println(cost.toString() + " Cost");
					
					BigDecimal revenue = otherBid.get(0).multiply(limitSize(otherBid.get(1), otherBid.get(0), money)); //multiply the bid price by the bid quantity
					
					//System.out.println(revenue.toString() + " Revenue");
					
					BigDecimal profit = revenue.subtract(cost);
					
					//System.out.println();
					
					totalCost = totalCost.add(cost);
					totalProfit = totalProfit.add(profit);
					money = money.subtract(cost);
				}
			}//end if
			
		}//end while
		totalCost = new BigDecimal(totalCost.toString());
		return totalProfit;
		
	}
	
	public BigDecimal getTotalCost()
	{
		return totalCost;
	}
	
	public BigDecimal limitSize(BigDecimal btc, BigDecimal btc_usd, BigDecimal limit)
	{
		if(btc.multiply(btc_usd).compareTo(limit) == 1)
		{
			return limit.divide(btc_usd, 8, RoundingMode.HALF_UP);
		}
		else
		{
			return btc;
		}
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("asks: ");
		for(ArrayList<BigDecimal> a : asks)
		{
			builder.append(a.get(0) + ", " + a.get(1) + " ");
		}
		builder.append("\nbids: ");
		for(ArrayList<BigDecimal> a : bids)
		{
			builder.append(a.get(0) + ", " + a.get(1) + " ");
		}
		return builder.toString();
	}
	
	/**
	 * gets the asks
	 * @return asks
	 */
	public ArrayList<ArrayList<BigDecimal>> getAsks(){
		return asks;
	}
	
	/**
	 * Gets the bids
	 * @return bids
	 */
	public ArrayList<ArrayList<BigDecimal>> getBids(){
		return bids;
	}
	
	
	
}
