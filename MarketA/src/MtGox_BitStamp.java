import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;


public class MtGox_BitStamp implements Runnable{

	
	@Override
	public void run(){
		BigDecimal last = new BigDecimal("-1");
		
		File csvData = new File("MtGox_BitStamp.csv");
		if(!csvData.exists()) {
		    try {
				csvData.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
		FileWriter fw = null;
		try {
			fw = new FileWriter(csvData, true);
		} catch (IOException e) {
			System.out.println("IoEXception");
			e.printStackTrace();
		} //create a file write Append = true.
		
		
		
		while(Main.getFlag())
		{
			BigDecimal MtGoxLAST = MarketData.getMtGox();//get MtGox Last price
			BigDecimal BitStampLAST = MarketData.getBitStamp(); //get bitstamp last price
			
			
			
			if(!last.equals(MtGoxLAST.subtract(BitStampLAST)) && (MtGoxLAST.compareTo(new BigDecimal(-1)) != 0) && (BitStampLAST.compareTo(new BigDecimal(-1)) != 0)){
				try {
					fw.write(MtGoxLAST + "," + BitStampLAST + "," + (MtGoxLAST.subtract(BitStampLAST)) + "," + (System.currentTimeMillis()/1000) + "\n");
				} catch (IOException e) {
					System.out.println("IOException");
					e.printStackTrace();
				} //write data to file
				System.out.println(MtGoxLAST + " " + BitStampLAST + " " + (MtGoxLAST.subtract(BitStampLAST))); //Print to console
				last = MtGoxLAST.subtract(BitStampLAST);
			}//end if
			
			if((MtGoxLAST.compareTo(new BigDecimal(-1)) == 0) || (BitStampLAST.compareTo(new BigDecimal(-1)) == 0))
				System.out.println("Error Data not retrieved successfuly");
			
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}//wait
			
			try {
				fw.flush();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}//end while
		
		try {
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}//close the FileWriter
		
	}

}
