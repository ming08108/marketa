
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;


public class Main implements Runnable {
	
	
	static boolean flag = true; //should I quit?
	
	public static boolean getFlag()
	{
		return flag;
	}
	
	public static void main(String args[]) throws IOException, InterruptedException
	{
		
		Thread StopT = new Thread(new Main());
		StopT.start();
		
		Thread MtGox_BitStamp = new Thread(new MtGox_BitStamp()); //logs the MtGox_BitStamp Pair
		MtGox_BitStamp.start();
		
		Thread MtGox_BTCe = new Thread(new MtGox_BTCe()); //logs the MtGox_BTCe Prices pair
		MtGox_BTCe.start();
		
		Thread OrderBookGetter = new Thread(new OrderBookGetter()); //order book checker
		OrderBookGetter.start();
		
		/*
		ArrayList<ArrayList<BigDecimal>> asks = new ArrayList<ArrayList<BigDecimal>>();
		ArrayList<BigDecimal> a1 = new ArrayList<BigDecimal>(); a1.add(new BigDecimal(70)); a1.add(new BigDecimal(3));
		ArrayList<BigDecimal> b1 = new ArrayList<BigDecimal>(); b1.add(new BigDecimal(80)); b1.add(new BigDecimal(2));
		ArrayList<BigDecimal> c1 = new ArrayList<BigDecimal>(); c1.add(new BigDecimal(85)); c1.add(new BigDecimal(2));
		
		asks.add(a1); asks.add(b1); asks.add(c1);
		
		
		ArrayList<ArrayList<BigDecimal>> bids = new ArrayList<ArrayList<BigDecimal>>();
		ArrayList<BigDecimal> a2 = new ArrayList<BigDecimal>(); a2.add(new BigDecimal(110)); a2.add(new BigDecimal(4));
		ArrayList<BigDecimal> b2 = new ArrayList<BigDecimal>(); b2.add(new BigDecimal(100)); b2.add(new BigDecimal(3));
		ArrayList<BigDecimal> c2 = new ArrayList<BigDecimal>(); c2.add(new BigDecimal(90)); c2.add(new BigDecimal(2));
		
		bids.add(a2); bids.add(b2); bids.add(c2);
		
		OrderBook test = new OrderBook(asks, bids);
		OrderBook test2 = new OrderBook(asks, bids);
		
		System.out.println(test.arbitroll(test2, new BigDecimal(0)));
		*/
		
		
	}//end main()

	@Override
	public void run() {
		System.out.println("Press Q + ENTER to close");
		Scanner kbIn = new Scanner(System.in);
		while(true)
		{
			if(kbIn.nextLine().equals("q"))
			{
				flag = false;
				kbIn.close();
				break;
			}//end if
		}//end while
	}//end run()
}


